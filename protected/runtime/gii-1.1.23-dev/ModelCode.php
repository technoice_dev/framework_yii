<?php
return array (
  'template' => 'default',
  'connectionId' => 'db',
  'tablePrefix' => 'tbl_test',
  'modelPath' => 'application.models',
  'baseClass' => 'CActiveRecord',
  'buildRelations' => '1',
  'commentsAsLabels' => '0',
);
